# Для дальнейшего образования

- Литература и полезные ссылки
    - [C# Notes for Professionals book](https://books.goalkicker.com/CSharpBook/) (Free)  
    - [C# documentation. Microsoft Documentation](https://docs.microsoft.com/en-us/dotnet/csharp/)          
    - [**Programming C# 8.0. by Ian Griffiths. O'Reilly Media. 2019**](https://www.oreilly.com/library/view/programming-c-80/9781492056805/) with [Code Listings](https://github.com/idg10/prog-cs-8-examples)
    - [C# 8.0 in a Nutshell. by Joseph Albahari, Eric Johannsen. 2020](https://www.oreilly.com/library/view/c-80-in/9781492051121/) with [Code Listings](http://www.albahari.com/nutshell/code.aspx) 
- Практика
    - https://www.codewars.com/kata/search/csharp?q=&&tags=Object-oriented%20Programming&beta=false&order_by=rank_id%20asc (`рекомендовано для выполнения`)
    - **https://github.com/epam-dotnet-lab/file-cabinet-task** (`обязательно для выполнения`)
